package com.kafka.demo.consumer;

import com.kafka.demo.model.Greeting;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class KafkaConsumer {

    @Autowired
    private KafkaTemplate<String, String> kafkaTemplate;

    @Autowired
    private KafkaTemplate<String, Greeting> greetingKafkaTemplate;

    @Value(value = "${message.topic.name}")
    private String topicName;

    @KafkaListener(topics = "${message.topic.name}", containerFactory = "greetingKafkaListenerContainerFactory")
    public void listenGreeting(Greeting greeting) {
        log.info("listenGreeting -- Message received from topic: {}", greeting);
    }
}
